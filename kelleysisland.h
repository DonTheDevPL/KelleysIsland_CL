class ECCorrections
{
	name="Erie County Corrections";
	position[]={2835.97,4577.44};
	type="StrongpointArea";
			
	radiusA=206.23;
	radiusB=113.64;
	angle=0.000;
};
class GraveSite
{
	name="Grave Site";
	position[]={3802.81,4464.64};
	type="ViewPoint";
		
	radiusA=49.78;
	radiusB=103.07;
	angle=0.000;
};
class CedarPoint
{
	name="Cedar Point";
	position[]={6613.05,5999.59};
	type="ViewPoint";
		
	radiusA=239.89;
	radiusB=190.78;
	angle=0.000;
};
class UnionCity
{
	name="Union City";
	position[]={3654.20,3165.72};
	type="NameCity";
		
	radiusA=388.67;
	radiusB=340.93;
	angle=9.182;
};
class LongBeach
{
	name="Long Beach";
	position[]={3397.50,2640.78};
	type="ViewPoint";
		
	radiusA=124.11;
	radiusB=46.42;
	angle=0.000;
};
class ErieCountyAirport
{
	name="Erie County Airport";
	position[]={5437.69,3735.81};
	type="Airport";
		
	radiusA=131.77;
	radiusB=68.91;
	angle=0.000;
};
class Heathrow
{
	name="Heathrow";
	position[]={2665.39,4119.69};
	type="NameVillage";
			
	radiusA=32.94;
	radiusB=19.37;
	angle=0.000;
};
class WatkinsGlen
{
	name="Watkins Glen";
	position[]={4004.13,4314.22};
	type="NameVillage";
			
	radiusA=208.27;
	radiusB=155.20;
	angle=0.000;
};
class WoodlandHeights
{
	name="Woodland Heights";
	position[]={3918.38,4841.01};
	type="NameVillage";
			
	radiusA=687.26;
	radiusB=512.13;
	angle=0.000;
};
class BelleValley
{
	name="Belle Valley";
	position[]={3123.42,5129.56};
	type="FlatArea";
			
	radiusA=600.36;
	radiusB=447.37;
	angle=0.000;
};
class LonePineBay
{
	name="Lone Pine Bay";
	position[]={1864.38,3279.17};
	type="NameLocal";
			
	radiusA=819.46;
	radiusB=610.64;
	angle=0.000;
};
class CostaDelMar
{
	name="Costa Del Mar";
	position[]={1536.39,3753.26};
	type="Hill";
					
	radiusA=894.83;
	radiusB=666.80;
	angle=0.000;
};
class ThunderBay
{
	name="Thunder Bay";
	position[]={4129.52,2308.75};
	type="NameMarine";
			
	radiusA=993.14;
	radiusB=740.06;
	angle=0.000;
};
class DartmouthAirField
{
	name="Dartmouth Air Field";
	position[]={6462.39,5646.83};
	type="Airport";
				
	radiusA=1355.59;
	radiusB=1010.15;
	angle=0.000;
};
class AirportTerminal
{
	name="Airport Terminal";
	position[]={5334.22,3395.85};
	type="ViewPoint";
			
	radiusA=606.31;
	radiusB=451.81;
	angle=0.000;
};
class UptownCrescent
{
	name="Uptown Crescent";
	position[]={5552.88,4394.79};
	type="CityCenter";
				
	radiusA=1034.49;
	radiusB=770.87;
	angle=0.000;
};
class SanAndino
{
	name="San Andino";
	position[]={5034.61,2224.46};
	type="NameLocal";
			
	radiusA=1129.62;
	radiusB=841.76;
	angle=0.000;
};
class Waterford
{
	name="Waterford";
	position[]={2875.56,3838.61};
	type="NameVillage";
			
	radiusA=1541.89;
	radiusB=1148.97;
	angle=0.000;
};
class MarsHill
{
	name="Mars Hill";
	position[]={4244.95,3458.51};
	type="Hill";
		
	radiusA=1073.84;
	radiusB=800.20;
	angle=0.000;
};
