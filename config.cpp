#define _ARMA_

class CfgPatches
{
  class kelleysisland
    {
      units[] = {};
      worlds[] = {kelleysisland};
      weapons[] = {};
      requiredVersion = 1.0;
      requiredaddons[]=
      {
        "A3_Data_F",
        "A3_Roads_F",
        "mm_bank",
        "mm_buildings",
        "mm_buildings2",
        "mm_buildings3",
        "mm_buildings4",
        "mm_post",
        "MM_objects",
        "mm_residential",
        "mm_residential2",
        "MM_Shopping",
        "mm_showroom",
        "EMOGLOBINSKY",
        "Charlieco89_CSP"
      };
      version = "26.02.2017";
      fileName = "kelleysisland.pbo";
    };
};
class CfgWorlds
{
  class CAWorld;
  class Stratis: CAWorld
 {
    class Grid;
    class DefaultClutter;
 };
 class kelleysisland: Stratis
 {
    cutscenes[] = {};
    description = "Kelleys Island";
    pictureMap = "KelleysIsland\data\kelleysisland_map.paa";
    worldName = "KelleysIsland\kelleysisland.wrp";
    startTime = "11:00";
    startDate = "05/03/2030";
    startWeather = 0.2;
    startFog = 0.0;
    forecastWeather = 0.6;
    forecastFog = 0.0;
    centerPosition[] = {4411,3554,65};
    seagullPos[] = {1024,1024,500};
    longitude = 65;
    latitude = -34;
    elevationOffset = 5;
    envTexture = "A3\Data_f\env_land_ca.tga";
    minTreesInForestSquare = 2;
    minRocksInRockSquare = 2;
    newRoadsShape = "KelleysIsland\data\roads\roads.shp";
    ilsPosition[] = {6043.55,3464.89};
    ilsDirection[] = {1,0.08,0};
    ilsTaxiIn[] = {5466,3661,5497,3713,5387,3736,5366,3737,5351,3754,5384,3773};
    ilsTaxiOff[] = {5361,3424,5436,3426,5467,3453,5468,3518,5466,3592,5466,3661};
    drawTaxiway = 1;
 class SecondaryAirports {};
class Sea
  {
   seaTexture = "a3\data_f\seatexture_co.paa";
   seaMaterial = "#water";
   shoreMaterial = "#shore";
   shoreFoamMaterial = "#shorefoam";
   shoreWetMaterial = "#shorewet";
   WaterMapScale = 20;
   WaterGrid = 50;
   MaxTide = 1;
   MaxWave = 2;
   SeaWaveXScale = "2.0/50";
   SeaWaveZScale = "1.0/50";
   SeaWaveHScale = 2.0;
   SeaWaveXDuration = 5000;
   SeaWaveZDuration = 10000;
  };
  class Grid: Grid
  {
   offsetX = 0;
   offsetY = 5120;
   class Zoom1
   {
    zoomMax = 0.15;
    format = "XY";
    formatX = "000";
    formatY = "000";
    stepX = 100;
    stepY = -100;
   };
   class Zoom2
   {
    zoomMax = 0.85;
    format = "XY";
    formatX = "00";
    formatY = "00";
    stepX = 1000;
    stepY = -1000;
   };
   class Zoom3
   {
    zoomMax = 1e+030.0;
    format = "XY";
    formatX = "0";
    formatY = "0";
    stepX = 10000;
    stepY = -10000;
   };
  };
  class Ambient{};
    class AmbientA3
    {
      maxCost = 500;
      class Radius440_500
      {
        areaSpawnRadius = 440.0;
        areaMaxRadius = 500.0;
        spawnCircleRadius = 30.0;
        spawnInterval = 4.7;
        class Species
        {
          class Kestrel_random_F
          {
            maxCircleCount = "((1 - night) * 2 * (1 - (WaterDepth interpolate [1,30,0,1])) + (2 * (hills))) * (1 - night)";
            maxWorldCount = 4;
            cost = 3;
            spawnCount = 1;
            groupSpawnRadius = 10;
            maxAlt = 200;
            minAlt = -10;
          };
          class Seagull
          {
            maxCircleCount = "((sea * (1 - night)) + (2 * houses * sea)) * (1 - night)";
            maxWorldCount = 8;
            cost = 3;
            spawnCount = 1;
            groupSpawnRadius = 10;
            maxAlt = 200;
            minAlt = -10;
          };
          class Rabbit_F
          {
            maxCircleCount = "(20 * (0.1 - houses)) * (1 - sea)";
            maxWorldCount = 4;
            cost = 5;
            spawnCount = 1;
            groupSpawnRadius = 10;
            maxAlt = 80;
            minAlt = -5;
          };
          class Goat_random_F
          {
            maxCircleCount = "(20 * (1 - night)) * (1 - sea) * (1 * hills)";
            maxWorldCount = 20;
            cost = 2;
            spawnCount = 3;
            groupSpawnRadius = 10;
            maxAlt = 80;
            minAlt = -5;
          };
          class Alsatian_Random_F
          {
            maxCircleCount = "(20 * (1 - night)) * (1 - sea) * (1 * hills)";
            maxWorldCount = 20;
            cost = 2;
            spawnCount = 3;
            groupSpawnRadius = 10;
            maxAlt = 80;
            minAlt = -5;
          };
          class Fin_random_F
          {
            maxCircleCount = "(20 * (1 - night)) * (1 - sea) * (1 * hills)";
            maxWorldCount = 20;
            cost = 2;
            spawnCount = 3;
            groupSpawnRadius = 10;
            maxAlt = 80;
            minAlt = -5;
          };
        };
      };
        class Radius40_60
      {
        areaSpawnRadius = 50.0;
        areaMaxRadius = 83.0;
        spawnCircleRadius = 10.0;
        spawnInterval = 1.5;
        class Species
        {
          class CatShark_F
          {
            maxCircleCount = "(4 * (WaterDepth interpolate [1,30,0,1]))";
            maxWorldCount = 10;
            cost = 6;
            spawnCount = 1;
            groupSpawnRadius = 10;
            maxAlt = 10;
            minAlt = -80;
          };
          class Turtle_F
          {
            maxCircleCount = "(2 * (waterDepth interpolate [1,16,0,1]) * ((1-houses) * (1-houses)))";
            maxWorldCount = 6;
            cost = 5;
            spawnCount = 1;
            groupSpawnRadius = 10;
            maxAlt = 10;
            minAlt = -80;
          };
          class Snake_random_F
          {
            maxCircleCount = "(1 - houses) * ((2 * (1 - sea)) + (2 * (meadow)))";
            maxWorldCount = 3;
            cost = 5;
            spawnCount = 1;
            groupSpawnRadius = 5;
            maxAlt = 40;
            minAlt = -5;
          };
          class Salema_F
          {
            maxCircleCount = "(12 * ((WaterDepth interpolate [1,30,0,1]) + 0.07))";
            maxWorldCount = 40;
            cost = 5;
            spawnCount = 2;
            groupSpawnRadius = 5;
            maxAlt = 10;
            minAlt = -80;
          };
          class Ornate_random_F
          {
            maxCircleCount = "(12 * ((WaterDepth interpolate [1,30,0,1]) + 0.05))";
            maxWorldCount = 30;
            cost = 5;
            spawnCount = 3;
            groupSpawnRadius = 5;
            maxAlt = 10;
            minAlt = -80;
          };
          class Mackerel_F
          {
            maxCircleCount = "(8 * ((WaterDepth interpolate [1,30,0,1]) + 0.07))";
            maxWorldCount = 14;
            cost = 5;
            spawnCount = 2;
            groupSpawnRadius = 5;
            maxAlt = 10;
            minAlt = -80;
          };
          class Mullet_F
          {
            maxCircleCount = "(8 * ((WaterDepth interpolate [1,30,0,1]) + 0.07))";
            maxWorldCount = 14;
            cost = 5;
            spawnCount = 2;
            groupSpawnRadius = 5;
            maxAlt = 10;
            minAlt = -80;
          };
          class Tuna_F
          {
            maxCircleCount = "(8 * ((WaterDepth interpolate [1,30,0,1]) - 0.2))";
            maxWorldCount = 10;
            cost = 5;
            spawnCount = 2;
            groupSpawnRadius = 5;
            maxAlt = 10;
            minAlt = -80;
          };
        };
      };
      class Radius30_40
      {
        areaSpawnRadius = 30.0;
        areaMaxRadius = 40.0;
        spawnCircleRadius = 3.0;
        spawnInterval = 3.75;
        class Species
        {
          class DragonFly
          {
            maxCircleCount = "4 * (1 - night) * (1 - (WaterDepth interpolate [1,30,0,1])) * sea * (1 - windy)";
            maxWorldCount = 4;
            cost = 1;
            spawnCount = 1;
            groupSpawnRadius = 1;
            maxAlt = 30;
            minAlt = -5;
          };
          class ButterFly_random
          {
            maxCircleCount = "3 * (1 - night) * (1 - (WaterDepth interpolate [1,30,0,1])) * (1 - windy)";
            maxWorldCount = 6;
            cost = 1;
            spawnCount = 3;
            groupSpawnRadius = 1;
            maxAlt = 30;
            minAlt = -5;
          };
          class FireFly
          {
            maxCircleCount = "(6 * night) * (1 - sea) * (1 - houses) * (1 - windy)";
            maxWorldCount = 20;
            cost = 1;
            spawnCount = 3;
            groupSpawnRadius = 3;
            maxAlt = 30;
            minAlt = -5;
          };
          class Cicada
          {
            maxCircleCount = "(4 * night) * (1 - sea) * (1 - windy)";
            maxWorldCount = 6;
            cost = 1;
            spawnCount = 3;
            groupSpawnRadius = 3;
            maxAlt = 30;
            minAlt = -5;
          };
        };
      };
    };
  #include "cfgClutter.hpp"
		class Names
		{
			#include "kelleysisland.h"
		};
    loadingTexts[] = {"Edited by noms to have no mod dependencies","Creative base by Andino Mitrell", "Help and suggestions by A3 Terrain Skype Group", "Kelley's Island is an actual Island in Lake Erie US","Special thanks to CypeRevenge for getting my map terrain started","Edited by DonTheDev for countylife.pl"};
 };
};
class CfgWorldList
{
 class kelleysisland{};
};
class CfgMissions
{
 class Cutscenes
 {};
};


//SURFACES
#include "cfgSurfaces.hpp"
