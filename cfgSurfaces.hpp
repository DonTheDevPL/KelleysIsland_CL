class CfgSurfaces
{
	class Default {};
	class kelleysisland_dirt_Surface : Default
	{
		 files = "kelleysisland_dirt_*";
		 rough = 0.09;
		 maxSpeedCoef = 0.9;
		 dust = 0.5;
		 soundEnviron = "dirt";
		 character = "Empty";
		 soundHit = "soft_ground";
		 lucidity = 1;
		 grassCover = 0.03;
	};
	class kelleysisland_dry_grass_Surface : Default
	{
		 files = "kelleysisland_dry_grass_*";
		 rough = 0.08;
		 maxSpeedCoef = 0.9;
		 dust = 0.75;
		 soundEnviron = "drygrass";
		 character = "kelleysisland_dry_grass_Character";
		soundHit = "soft_ground";
		 lucidity = 2;
		 grassCover = 0.1;
	};
	class kelleysisland_forest_pine_Surface : Default
	{
		 files = "kelleysisland_forest_pine_*";
		 rough = 0.12;
		 maxSpeedCoef = 0.8;
		 dust = 0.4;
		 soundEnviron = "drygrass";
		 character = "kelleysisland_forest_pine_Character";
		 soundHit = "soft_ground";
		 lucidity = 3.5;
		 grassCover = 0.04;
	};
	class kelleysisland_grass_green_Surface : Default
	{
		 files = "kelleysisland_grass_green_*";
		 rough = 0.08;
		 maxSpeedCoef = 0.9;
		 dust = 0.15;
		 soundEnviron = "grass";
		 character = "kelleysisland_green_grass_Character";
		 soundHit = "soft_ground";
		 lucidity = 4;
		 grassCover = 0.05;
	};
	class kelleysisland_soil_Surface : Default
	{
		 files = "kelleysisland_soil_*";
		 rough = 0.09;
		 maxSpeedCoef = 0.9;
		 dust = 0.5;
		 soundEnviron = "dirt";
		 character = "Empty";
		 soundHit = "hard_ground";
		 lucidity = 1;
		 grassCover = 0.0;
	};
	class kelleysisland_concrete_Surface : Default
	{
		 files = "kelleysisland_concrete_*";
		 rough = 0.09;
		 maxSpeedCoef = 0.9;
	 	 dust = 0.5;
		 soundEnviron = "concrete_ext";
		 character = "Empty";
		 soundHit = "hard_ground";
		 lucidity = 0;
		 grassCover = 0.0;
	};
	class kelleysisland_beach_Surface : Default
	{
		 files = "kelleysisland_beach_*";
		 rough = 0.08;
		 maxSpeedCoef = 0.9;
		 dust = 0.75;
		 soundEnviron = "sand";
		 character = "Empty";
		 soundHit = "soft_ground";
		 lucidity = 6;
		 grassCover = 0.0;
	};
	class kelleysisland_seabed_Surface : Default
	{
		 files = "kelleysisland_seabed_*";
		 rough = 0.12;
		 maxSpeedCoef = 0.85;
		 dust = 0.0;
		 soundEnviron = "gravel";
		 character = "kelleysisland_seabed_Character";
		 soundHit = "soft_ground";
		 lucidity = 1.5;
		 grassCover = 0.0;
	};
};

class CfgSurfaceCharacters
{
    class kelleysisland_forest_pine_Character
	{
		probability[] = {0.05,0.012,0.01};
		names[] = {"kelleysisland_BigFallenBranches_pine","kelleysisland_GrassDryGroup","kelleysisland_GrassGreenGroup"};
	};
	class kelleysisland_green_grass_Character
  	{
		probability[] = {0.60};
		names[] = {"kelleysisland_GrassGreenGroup"};
  	};
	class kelleysisland_dry_grass_Character
  	{
		probability[] = {0.21,0.26,0.21,0.09,0.06,0.07,0.05,0.04};
		names[] = {"kelleysisland_GrassDryGroup","kelleysisland_GrassDryMediumgroup","kelleysisland_WeedBrownTallGroup","kelleysisland_WeedGreenTall","kelleysisland_ThistleYellowShrub","kelleysisland_PlantMullein","kelleysisland_ThistleThornGreenSmall","kelleysisland_ThistleThornBrownSmall"};
  	};
  	class kelleysisland_seabed_Character
  	{
  		probability[] = {0.5,0.1,0.1,0.1,0.2};
  		names[] = {"kelleysisland_CoralPlant","kelleysisland_CoralTurds","kelleysisland_CoralSalad","kelleysisland_CoralGrass","kelleysisland_CoralBalls"};
  	};
};