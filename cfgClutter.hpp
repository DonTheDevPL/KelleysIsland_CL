class Clutter
{
   class kelleysisland_BigFallenBranches_pine: DefaultClutter
	{
		model = "A3\Plants_F\Clutter\c_bigFallenBranches_pine.p3d";
		affectedByWind = 0.0;
		swLighting = "false";
		scaleMin = 0.3;
		scaleMax = 0.7;
	};
   class kelleysisland_BigFallenBranches_pine02: DefaultClutter
	{
		model = "A3\Plants_F\Clutter\c_bigFallenBranches_pine02.p3d";
		affectedByWind = 0.0;
		swLighting = "false";
		scaleMin = 0.3;
		scaleMax = 0.7;
	};
   class kelleysisland_BigFallenBranches_pine03: DefaultClutter
	{
		model = "A3\Plants_F\Clutter\c_bigFallenBranches_pine03.p3d";
		affectedByWind = 0.0;
		swLighting = "false";
		scaleMin = 0.3;
		scaleMax = 0.7;
	};
   class kelleysisland_GrassGreenGroup: DefaultClutter
	{
		model = "A3\plants_f\Clutter\c_StrGrassGreen_group.p3d";
		affectedByWind = 0.6;
		swLighting = "true";
		scaleMin = 0.7;
		scaleMax = 1.0;
	};
   class kelleysisland_GrassDry: DefaultClutter
	{
		model = "A3\plants_f\Clutter\c_StrGrassDry.p3d";
		affectedByWind = 0.5;
		swLighting = "true";
		scaleMin = 0.8;
		scaleMax = 1.2;
	};
   class kelleysisland_GrassDryGroup: DefaultClutter
	{
		model = "A3\plants_f\Clutter\c_StrGrassDry_group.p3d";
		affectedByWind = 0.65;
		swLighting = "true";
		scaleMin = 0.65;
		scaleMax = 1.0;
	};
   class kelleysisland_GrassDryMediumGroup: DefaultClutter
	{
		model = "A3\plants_f\Clutter\c_StrGrassDryMedium_group.p3d";
		affectedByWind = 0.7;
		swLighting = "true";
		scaleMin = 0.8;
		scaleMax = 1.0;
	};
   class kelleysisland_WeedBrownTallGroup: DefaultClutter
	{
		model = "A3\plants_f\Clutter\c_StrWeedBrownTall_group.p3d";
		affectedByWind = 0.3;
		swLighting = "true";
		scaleMin = 0.9;
		scaleMax = 1.25;
	};
   class kelleysisland_WeedGreenTall: DefaultClutter
	{
		model = "A3\plants_f\Clutter\c_StrWeedGreenTall.p3d";
		affectedByWind = 0.3;
		swLighting = "true";
		scaleMin = 0.8;
		scaleMax = 1.2;
	};
   class kelleysisland_PlantMullein: DefaultClutter
	{
		model = "A3\plants_f\Clutter\c_StrPlantMullein.p3d";
		affectedByWind = 0.35;
		swLighting = "true";
		scaleMin = 0.7;
		scaleMax = 1.15;
	};
   class kelleysisland_ThistleYellowShrub: DefaultClutter
	{
		model = "A3\plants_f\Clutter\c_StrThistleYellowShrub.p3d";
		affectedByWind = 0.2;
		swLighting = "true";
		scaleMin = 0.7;
		scaleMax = 1.1;
	};
   class kelleysisland_ThistleThornGreen: DefaultClutter
	{
		model = "A3\plants_f\Clutter\c_Thistle_Thorn_Green.p3d";
		affectedByWind = 0.3;
		swLighting = "false";
		scaleMin = 0.3;
		scaleMax = 1.0;
	};
   class kelleysisland_ThistleThornGreenSmall: DefaultClutter
	{
		model = "A3\plants_f\Clutter\c_Thistle_Thorn_Green.p3d";
		affectedByWind = 0.25;
		swLighting = "false";
		scaleMin = 0.4;
		scaleMax = 0.7;
	};
   class kelleysisland_FlowerLowYellow2: DefaultClutter
	{
		model = "A3\plants_f\Clutter\c_Flower_Low_Yellow2.p3d";
		affectedByWind = 0.4;
		swLighting = "true";
		scaleMin = 0.6;
		scaleMax = 1.0;
	};
	class kelleysisland_ThistleThornBrownSmall: DefaultClutter
	{
		model = "A3\plants_f\Clutter\c_Thistle_Thorn_Brown.p3d";
		affectedByWind = 0.25;
		swLighting = "false";
		scaleMin = 0.7;
		scaleMax = 1.0;
	};
	class kelleysisland_StrPlantGreenSmall: DefaultClutter
	{
		model = "A3\plants_f\Clutter\c_StrPlantGreenSmall.p3d";
		affectedByWind = 0.25;
		swLighting = "false";
		scaleMin = 0.5;
		scaleMax = 0.9;
	};
	class kelleysisland_CoralPlant: DefaultClutter
	{
		model = "A3\plants_f\Clutter\c_coral1.p3d";
		affectedByWind = 0.2;
		scaleMin = 0.85;
		scaleMax = 1.25;
	};
	class kelleysisland_CoralTurd: DefaultClutter
	{
		model = "A3\plants_f\Clutter\c_coral_brain_flat1.p3d";
		affectedByWind = 0;
		scaleMin = 0.95;
		scaleMax = 1.45;
	};
	class kelleysisland_CoralTurds: DefaultClutter
	{
		model = "A3\plants_f\Clutter\c_corals_set_t.p3d";
		affectedByWind = 0;
		scaleMin = 0.85;
		scaleMax = 1.25;
	};
	class kelleysisland_CoralSalad: DefaultClutter
		{
		model = "A3\plants_f\Clutter\c_coral_plant_set1.p3d";
		affectedByWind = 0;
		scaleMin = 0.95;
		scaleMax = 1.35;
	};
	class kelleysisland_CoralGrass: DefaultClutter
	{
		model = "A3\plants_f\Clutter\c_coral3.p3d";
		affectedByWind = 0.2;
		scaleMin = 0.5;
		scaleMax = 1.5;
	};
	class kelleysisland_CoralBalls: DefaultClutter
	{
		model = "A3\plants_f\Clutter\c_coral_brain_flat_set1.p3d";
		affectedByWind = 0;
		scaleMin = 0.5;
		scaleMax = 1.1;
	};
};